﻿using ChromeTest.DriverManager;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace ChromeTest.Core
{
    public class Waits      //в методе ожидания созд объект класса уэйт, который принимает экз дравйвера
    {
        IWebDriver InsDriver = Driver.GetInstance().CurrentDriver;




        public IWebElement WaitForVisible(By locator) //есть в доме и видим на странице
        {


            WebDriverWait wait = new WebDriverWait(InsDriver, TimeSpan.FromSeconds(5));
            return wait.Until(ExpectedConditions.ElementIsVisible(locator));
             
        }

        public IWebElement WaitForClickable(By locator)  //есть в доме и можно кликнуть
        {
            WebDriverWait wait = new WebDriverWait(InsDriver, TimeSpan.FromSeconds(5));
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait.Until(ExpectedConditions.ElementToBeClickable(locator));
        }

        public IWebElement WaitForExists(By locator)
        {
            WebDriverWait wait = new WebDriverWait(InsDriver, TimeSpan.FromSeconds(5));
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            return wait.Until(ExpectedConditions.ElementExists(locator));
        }

       
       
    }
}
