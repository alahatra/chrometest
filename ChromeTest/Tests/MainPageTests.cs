using ChromeTest;
using ChromeTest.DriverManager;
using ChromeTest.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;

namespace Tests
{

    
    public class Tests : BaseTest
    {

        MainPageObject mainPage = new MainPageObject();
        SearchResultPageObject resultPage = new SearchResultPageObject();

        string searchText = "������ ���� USD";

        string resultPageUrl = "https://search.tut.by/";
        string header = "�����";

        [Test]
        
        public void VerifyTextInSearchFieldTest()
        {
            
            mainPage.EnterSearchTextToSearchField(searchText);
       
            Assert.IsTrue(mainPage.CheckOutTextIsDisplayedInSearchField(searchText), "Text in searchField doesn't match");

            //resultPage.ClickFirtsLinkOnResultsPage();

        }

        [Test]

        public void PageWithSearchResultsIsDisplayed()
        {
            mainPage.EnterSearchTextToSearchField(searchText);
            mainPage.ClickSearchButton();
            string searchPageUrl = resultPage.GetCurrentUrl();

            Assert.IsTrue(searchPageUrl.Contains(resultPageUrl), searchPageUrl, "WrongPage");

            
        }

        
    }
}