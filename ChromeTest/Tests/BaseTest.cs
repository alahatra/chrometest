﻿using ChromeTest.Pages;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace ChromeTest.DriverManager
{
    public class BaseTest
    {

        protected Driver driver = Driver.GetInstance();

        BasePageObject basePage = new BasePageObject();

        [OneTimeSetUp]

        public void OneTimeSetUp()
        {

        }

        [SetUp]

        public void OpenBrowser()
        {
            basePage.GoToPage("https://www.tut.by/");
        }

        [OneTimeTearDown]

        public void TearDown()
        {
            basePage.CloseBrowser();
        }
    }
}
