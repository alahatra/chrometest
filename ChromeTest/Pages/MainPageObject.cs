﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChromeTest.Pages
{
    public class MainPageObject:BasePageObject
    {
        
               

        private readonly By _searchFieldLocator = By.Id("search_from_str");
        private readonly By _searchButtonLocator = By.Name("search");

       
        

        public void EnterSearchTextToSearchField(string searchText)
        {
            FindElement(_searchFieldLocator);
            ClearSendKeys(_searchFieldLocator, searchText);
            
        }

        public bool CheckOutTextIsDisplayedInSearchField(string searchText)
        {
            return  GetAttribute(_searchFieldLocator, searchText) == searchText;
            
        }

        public string SendAttribute(string searchText)
        {
            return GetAttribute(_searchFieldLocator, searchText);
        }

        public void ClickSearchButton()
        {
            FindElement(_searchButtonLocator);
            ButtonClick(_searchButtonLocator);

        }

        

        

        
    }
}
