﻿using System;
using System.Collections.Generic;
using System.Text;
using ChromeTest.Core;
using ChromeTest.DriverManager;
using OpenQA.Selenium;

namespace ChromeTest.Pages
{
    public class BasePageObject: Waits
    {
        IWebDriver InsDriver = Driver.GetInstance().CurrentDriver;

        
        public void ClearSendKeys(By locator, string text)
        {
            WaitForVisible(locator).Clear();
            WaitForVisible(locator).SendKeys(text);

        }

        public void FindElement(By locator)
        {
            WaitForExists(locator);
        }

        public void ButtonClick(By locator)
        {
            WaitForClickable(locator).Click();
            
        }

        public string  GetAttribute(By locator, string attributeName)
        {
            return WaitForExists(locator).GetAttribute(attributeName);
        }



        public void ButtonSubmit(By locator)
        {
            WaitForClickable(locator).Submit();
        }

        public void GoToPage(string url)
        {
            InsDriver.Navigate().GoToUrl(url);
        }

        public string GetUrl()
        {
            return InsDriver.Url;
        }

        
        public void CloseBrowser()
        {
            InsDriver.Quit();
        }
         
    }
}
