﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChromeTest.Pages
{
    class SearchResultPageObject: BasePageObject
    {
        IWebDriver InsDriver;

        private readonly By _firstLinkLocator = By.XPath("//li[contains(@class, 'b-results__li')]/h3/a[contains(@href, 'usd')]");
        private readonly By _pageHeaderLocator = By.XPath("//title[contains(text(), 'ПОИСК')]");

        public void ClickFirtsLinkOnResultsPage()
        {
            FindElement(_firstLinkLocator);
            ButtonClick(_firstLinkLocator);

        }

        public string GetCurrentUrl()
        {
            return GetUrl();
        }

        
        

        
    }
}
